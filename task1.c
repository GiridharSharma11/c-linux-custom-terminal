//Name: Giridhar Gopal Sharma
//StudentID: 29223709
//Last Modified: 30/08/19
//Task1
// The following program opens a file in the current directory and then 
// outputs only the last 200 characters present in the file, if the file is less than
// 200 characters, then the entire file is displayed  

// Including the necessary libraries for the program 
#include <string.h> 
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h> 
#include <sys/fcntl.h> 

int main(int argc, char *argv[]){
    // Initialising the variables used 
	int mainfile;// stores filename
    char buffer[200];//stores last 200 characters
    int read_size;//calculating the size while reading the file
	int write_size;//calculating the size while writing the file
	
	// opening the file in read format 
	if ((mainfile = open("logfile.txt", O_RDONLY)) < 0) {
		write(2, strerror(errno), strlen(strerror(errno)));
		exit(1);
	}// If the file size is less than 200, we print the whole file
    else if(read_size = read(mainfile, 200, sizeof(buffer))) <= 200)){
        // setting buffer to 200 characters and pointing to 200 chars from the end of file
        lseek(mainfile,200,SEEK_SET);
        // calculating the file size
        write_size = read(mainfile, 200, sizeof(buffer));
        // displaying the file contents 
        write(1, buffer, Write_sizesize);
        // close file and exit
	    close(mainfile);
    }
    else {
         // set lseek to read last 200 characters in the file
	    lseek(mainfile, -200, SEEK_END);
	    int size = read(mainfile, buffer, 200);
        // displaying the last 200 characters in the file
	    write(1, buffer, size);
        // closing the file and exit
    	close(mainfile);
    }
    // end of program, exiting....
	exit(0);
}