//Name: Giridhar Gopal Sharma
//StudentID: 29223709
//Last Modified: 30/08/19
//Task2
// The program allows the user to display any fixed number of characters in the file 
// entered by the user in the command line with the command -n followed by the 
// the integer(no of characters to be displayed from the last of the file) 

// Including the necessary libraries for the program 
#include <string.h> 
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h> 
#include <sys/fcntl.h> 

int tailfunc(char fd[], char buf[], int num_of_char){
	// creating a custom tail function to display the characters in the file 
    // specified by the user 
	int mainfile;
    // opening the file in read only mode
	if ((mainfile = open(fd, O_RDONLY)) < 0) {
		write(2, strerror(errno), strlen(strerror(errno)));
		exit(1);	
		}
        // setting buffer to user specified integer value from the end of the file
		lseek(mainfile, -num_of_char, SEEK_END);
        // calculating file size
		int size = read(mainfile, buf, num_of_char);
        // displaying the file contents
		write(1, buf, size);
        // closing file and exiting program
		close(mainfile);
        // enf of program, exit ......
		exit(0);	
}

int main(int argc, char *argv[]){
    // initialising the variables required
	char buffer[200];//storing the file contents
	int mainfile;// storing filename
	char arg1[] = "Hi! How many characters do you wish to display from the end of the file?";//user request
	char arg2[] = "Error!! Entered number should be non-negative.. Sorry!!!!";//error1
	char arg3[] = "Please enter non negative values!!!!!";//error2
	char arg4[] = "Sorry!!! Something went wrong with the entered argument.....";//error3
	char filename[] = "logfile.txt";
	// Opening the file in read format
    // having seperate case values using if else statements for different values provided in the argc(command line)
    // case 1
	if( argc == 1){
    // calling custom made tail function
	tailfunc(filename, buffer, 200);
	}// case2
	else if(argc == 2){
		if(strcmp(argv[1], "-n")==0){
			write(2, arg1,strlen(arg1));
			exit(1);
		}
		tailfunc(argv[1], buffer, 200);
	}
	else if(argc == 3){ // case3
		if(strcmp(argv[1],"-n")==0){
			int chars = atoi(argv[2]);
			if(chars == 0){
				write(2, arg3,strlen(arg3));
				exit(1);
			} 
			if(chars<1){
			    write(2, arg2,strlen(arg2));
				exit(1);
			}
			tailfunc(filename, buffer, chars);
		}
		if(strcmp(argv[2], "-n")==0){
		
			write(2, arg1,strlen(arg1));
			exit(1);
		}
		write(2, arg4,strlen(arg4));	
	}// case 4
	else if(argc == 4){
		
		if(strcmp(argv[3], "-n")==0){
		
			write(2, arg4,strlen(arg4));
			exit(1);
		}
	}
