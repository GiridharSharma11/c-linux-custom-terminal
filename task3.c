//Name: Giridhar Gopal Sharma
//StudentID: 29223709
//Last Modified: 30/08/19
//Task3
// The following program allows user to print the last 10 lines in a file
// using the -L argument, in case the file is less than 10 lines,
// all the contents of the file are displayed
// Citation : GeekforGeeks Printing the n number of lines 
//Including the necessary libraries for the program 
#include <string.h> 
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h> 
#include <sys/fcntl.h> 
#define DELIM   '\n' 

int tailfunc(char fd[], char buf[], int num_of_char){
	// creating a custom tail function to display the characters in the file 
    // specified by the user 
	int mainfile;
    // opening the file in read only mode
	if ((mainfile = open(fd, O_RDONLY)) < 0) {
		write(2, strerror(errno), strlen(strerror(errno)));
		exit(1);	
		}
        // setting buffer to user specified integer value from the end of the file
		lseek(mainfile, -num_of_char, SEEK_END);
        // calculating file size
		int size = read(mainfile, buf, num_of_char);
        // displaying the file contents
		write(1, buf, size);
        // closing file and exiting program
		close(mainfile);
        // enf of program, exit ......
		exit(0);	
}

int main(int argc, char *argv[]){
    // initialising the variables required
	char buffer[200];//storing the file contents
    int infile;
	int counter;
	int read_size, file_size;
    int n = 10;
    if((infile = open("logfile", O_RDONLY, 0664)) < 0)
	{
		perror("logfile");
		exit(1);
	}
    file_size = lseek(infile, 0, SEEK_END);	

    size_t cnt  = 0; // To store count of '\n' or DELIM 
    char *target_pos   = NULL; // To store the output position in str 

    if (target_pos == NULL) 
    { 
        perror("ERROR: string doesn't contain '\\n' character\n"); 
        return; 
    } 

    while (cnt <= n) 
    { 
        // Finding the next instance of '\n' 
        while (buffer < target_pos && *target_pos != DELIM) 
            --target_pos; 
  
         // skip '\n' and increment count of '\n'
        if (*target_pos ==  DELIM) 
            --target_pos, ++cnt; 
  
        // str < target_pos means str has less than 10 '\n' characters, so break from loop 
        else
            break; 
    } 

    if (str < target_pos) 
        target_pos += 2; 


    tailfunc(argv[1], target_pos, 10);;


}







